# godot-spawnable-tilemap
A custom TileMap that spawns objects in Godot Engine.

## Usage
1. Clone this repo into your project folder.
2. Click '**Add Child Node**' and select `SpawnableTileMap`.
3. Create or load a `TileSet`.
4. Create a JSON file with with the structure defined in the **Object JSON Format** section of this README:
5. Assign the JSON file to the TileMap.
6. You're done!

## Custom Properties
`SpawnableTileMap` supports custom properties. 
To use custom properties, extend from `SpawnableTileMap` and override `set_custom_properties(object, map_key)`.

Example `MySpawnableTileMap.gd`:
```
extends SpawnableTileMap

func set_custom_properties(object, map_key) -> void:
	if map_key.has("fruit_type") and object.has_method("set_fruit_type"):
		var k = map_key["fruit_type"]
		object.set_fruit_type(k)
	if map_key.has("my_second_property") and object.has_method("set_my_second_property"):
		var k = map_key["my_second_property"]
		object.set_my_second_property(k)
```

## Object JSON Format
```
{
    "apple": {
        "scene": "res://assets/objects/items/Fruit.tscn",
        "fruit_type": "apple"
    },
	"bananas": {
		"scene": "res://assets/objects/items/Fruit.tscn"
	}
}
```

## License
This project is heavily based on [tile-spawner](https://github.com/qtip/tile-spawner). See `LICENSE.txt` for more details.