extends TileMap
class_name SpawnableTileMap

# Inspector properties.
export(String, FILE, "*.json") var object_data

func _ready() -> void:
	# Load objects on the next frame.
	call_deferred("instance_objects")

# Creates instances of objects from a tilemap, using a mapping file.
func instance_objects() -> void:
	# Get the tile set from the tilemap.
	# Load the mapping to a dictionary.
	var object_map := get_object_dictionary(object_data)
	# Iterate the map's cells and create each object.
	for cell in get_used_cells():
		# Get the name of the object.
		var obj_name := tile_set.tile_get_name(get_cell(cell.x, cell.y))
		# Get the scene from the mapping.
		var map_key = object_map.get(obj_name)
		if map_key and map_key.has("scene"):
			var obj_scene = map_key["scene"]
			# Create an instance of the object.
			var object = load(obj_scene).instance()
		
			# Get the offset of the tile (if any).
			var tile_offset := Vector2()
			if map_key.has("offset_x"):
				tile_offset.x = map_key["offset_x"]
			if map_key.has("offset_y"):
				tile_offset.y = map_key["offset_y"]
			
			set_custom_properties(object, map_key)
			
			# Set the object's position and rotation.
			object.position = map_to_world(Vector2(cell.x, cell.y)) + tile_offset
			object.rotation_degrees = get_tile_rotation(self, cell)
			
			# Add as a child of the TileMap object and clear the tile cell.
			add_child(object)
			set_cell(cell.x, cell.y, -1)

# Loads a JSON file into a dictionary and returns the dictionary.
func get_object_dictionary(mapping : String) -> Dictionary:
	var dict := {}
	var file := File.new()
	if file.file_exists(mapping):
		file.open(mapping, file.READ)
		var text = file.get_as_text()
		dict = parse_json(text)
		file.close()
	else:
		print("Mapping file not found: %s" % mapping)
	return dict

# Returns the rotation of the current tile in degrees.
func get_tile_rotation(map : TileMap, cell : Vector2) -> float:
	# Get the properties needed to determine rotation.
	var transposed := map.is_cell_transposed(cell.x, cell.y)
	var flipped_x := map.is_cell_x_flipped(cell.x, cell.y)
	var flipped_y := map.is_cell_y_flipped(cell.x, cell.y)
	
	# 90 degree rotation is transpose and flip X.
	if transposed and flipped_x:
		return 90.0
	# 180 degree rotation is flip X and flip Y.
	if flipped_x and flipped_y:
		return 180.0
	# 270 degree rotation is transpose and flip Y.
	if transposed and flipped_y:
		return 270.0
	# No rotation defined.
	return 0.0

# Assign custom properties to the created object. Extend this class and override
# this function to set your own. Use the format provided in this function.
func set_custom_properties(object, map_key) -> void:
	if map_key.has("animation") and object.has_method("set_animation"):
		var k = map_key["animation"]
		object.set_animation(k)
